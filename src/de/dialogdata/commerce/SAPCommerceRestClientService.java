package de.dialogdata.commerce;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

public class SAPCommerceRestClientService {
	public enum DatabaseUpdateMode { NONE, UPDATE, INITIALIZE }
	public enum DeploymentStrategy { ROLLING_UPDATE, RECREATE }

	public static final String BASE_URL = "https://portalrotapi.hana.ondemand.com/v2/subscriptions/{0}";
	public static final String BASE_BUILDS_URL = BASE_URL + "/builds";
	public static final String GET_BUILD_URL = BASE_URL + "/builds/{1}";
	public static final String GET_BUILD_LOGS_URL = BASE_URL + "/builds/{1}/logs";
	public static final String GET_BUILD_PROGRESS_URL = BASE_URL + "/builds/{1}/progress";
	public static final String BASE_DEPLOYMENTS_URL = BASE_URL + "/deployments";
	public static final String GET_DEPLOYMENT_URL = BASE_URL + "/deployments/{1}";
	public static final String GET_DEPLOYMENT_PROGRESS_URL = BASE_URL + "/deployments/{1}/progress";
	public static final String CREATE_DEPLOYMENT_CANCELLATION_URL = BASE_URL + "/deployments/{1}/cancellation";
	public static final String GET_DEPLOYMENT_CANCELLATION_OPTIONS_URL = BASE_URL + "/deployments/{1}/cancellationoptions";

	private final RestTemplate restTemplate;
	private final Gson gson;
	private final ConfigurationService configurationService;

	public SAPCommerceRestClientService(ConfigurationService configurationService) {
		restTemplate = new RestTemplate();
		gson = new GsonBuilder().setPrettyPrinting().create();
		this.configurationService = configurationService;
	}

	public String getBuilds() {
		String url = buildUrl(BASE_BUILDS_URL);
		return doRequest(url, HttpMethod.GET, null);
	}

	public String createBuild(String branch, String name) {
		String url = buildUrl(BASE_BUILDS_URL);
		Map<String, String> requestMap = new HashMap<>();
		requestMap.put("branch", branch);
		requestMap.put("name", name);
		System.out.println(gson.toJson(requestMap));
		return gson.fromJson(doRequest(url, HttpMethod.POST, requestMap), Map.class).get("code").toString();
	}

	public Map<String, String> getBuild(String buildCode) {
		String url = buildUrl(GET_BUILD_URL, buildCode);
		return gson.fromJson(doRequest(url, HttpMethod.GET, null), Map.class);
	}

	public Map<String, String> getBuildProgress(String buildCode) {
		String url = buildUrl(GET_BUILD_PROGRESS_URL, buildCode);
		return gson.fromJson(doRequest(url, HttpMethod.GET, null), Map.class);
	}

	public String getDeployments() {
		String url = buildUrl(BASE_DEPLOYMENTS_URL);
		return doRequest(url, HttpMethod.GET, null);
	}

	public String createDeployment(String buildCode, String environment, DatabaseUpdateMode databaseUpdateMode, DeploymentStrategy deploymentStrategy) {
		String url = buildUrl(BASE_DEPLOYMENTS_URL);
		Map<String, String> requestMap = new HashMap<>();
		requestMap.put("buildCode", buildCode);
		requestMap.put("environmentCode", environment);
		requestMap.put("databaseUpdateMode", databaseUpdateMode.toString());
		requestMap.put("strategy", deploymentStrategy.toString());
		System.out.println(gson.toJson(requestMap));
		return gson.fromJson(doRequest(url, HttpMethod.POST, requestMap), Map.class).get("code").toString();
	}

	public Map<String, String> getDeploymentProgress(String deploymentCode) {
		String url = buildUrl(GET_DEPLOYMENT_PROGRESS_URL, deploymentCode);
		return gson.fromJson(doRequest(url, HttpMethod.GET, null), Map.class);
	}

	 private String buildUrl(String urlPattern, String ... params) {
		String subscriptionId = configurationService.getProperty(ConfigurationService.PROPERTY_SUBSCRIPTION_ID);

		String[] adjustedParams = new String[1 + params.length];
		adjustedParams[0] = subscriptionId;
		System.arraycopy(params, 0, adjustedParams, 1, params.length);

		return MessageFormat.format(urlPattern, adjustedParams);
	}

	private String doRequest(String endpointUrl, HttpMethod httpMethod, Map<String, String> requestMap) {
		String request = gson.toJson(requestMap);
		ResponseEntity<String> response = restTemplate.exchange(endpointUrl, httpMethod, new HttpEntity<String>(request, createHeaders()), String.class);
		return response.getBody();
	}

	private HttpHeaders createHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + configurationService.getProperty(ConfigurationService.PROPERTY_API_TOKEN));
		headers.set("Content-Type", "application/json");
		return headers;
	}
}
