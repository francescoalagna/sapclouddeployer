package de.dialogdata.commerce;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;


public class Main {
    public static final String BUILD_STATUS_SUCCESS = "SUCCESS";
    public static final String BUILD_STATUS_FAILED = "FAIL";
    public static final List<String> BUILD_EXIT_STATUS = Arrays.asList(BUILD_STATUS_SUCCESS, BUILD_STATUS_FAILED);

    public static final String DEPLOYMENT_STATUS_DEPLOYED = "DEPLOYED";
    public static final String DEPLOYMENT_STATUS_FAILED = "FAIL";
    public static final List<String> DEPLOYMENT_EXIT_STATUS = Arrays.asList(DEPLOYMENT_STATUS_DEPLOYED, DEPLOYMENT_STATUS_FAILED);

    public ConfigurationService configurationService;
    public SAPCommerceRestClientService sapCommerceRestClientService;

    public static void main(String[] args) throws Exception {
//        log(new SAPCommerceRestClientService(new ConfigurationService()).getDeployments());
        new Main().buildAndDeploy();
    }

    public void buildAndDeploy() throws Exception {
        configurationService = new ConfigurationService();
        sapCommerceRestClientService = new SAPCommerceRestClientService(configurationService);

        // Create build
        String targetBranch = configurationService.getProperty(ConfigurationService.PROPERTY_BUILD_BRANCH);
        String buildName = generateBuildName();
        log("Creating build " + buildName + " on branch " + targetBranch);
        String buildCode = sapCommerceRestClientService.createBuild(targetBranch, buildName);
//        String buildCode = "20200624.1";

        log("Build successfully started with buildCode " + buildCode);

        // Check build progress
        String buildStatus = null;
        Object buildProgress = null;
        while(true) {
            Map buildInfo = sapCommerceRestClientService.getBuildProgress(buildCode);
            String newBuildStatus = buildInfo.get("buildStatus").toString();
            Object newBuildProgress = buildInfo.get("percentage");

            if(!Objects.equals(buildStatus, newBuildStatus) || !Objects.equals(buildProgress, newBuildProgress)) {
                log("Build status: " + newBuildStatus + " (" + newBuildProgress + "%)");
                buildStatus = newBuildStatus;
                buildProgress = newBuildProgress;
            }
            if(BUILD_STATUS_FAILED.equals(buildStatus)) {
                log(buildInfo.toString());
                return;
            }
            if(BUILD_EXIT_STATUS.contains(buildStatus)) {
                break;
            }
            Thread.sleep(10000);
        }

        // Create deployment
        String environment = configurationService.getProperty(ConfigurationService.PROPERTY_DEPLOYMENT_ENVIRONMENT);
        SAPCommerceRestClientService.DatabaseUpdateMode dataMigrationMode = SAPCommerceRestClientService.DatabaseUpdateMode
                .valueOf(configurationService.getProperty(ConfigurationService.PROPERTY_DEPLOYMENT_DATAMIGRATION_MODE));
        SAPCommerceRestClientService.DeploymentStrategy deploymentMode = SAPCommerceRestClientService.DeploymentStrategy
                .valueOf(configurationService.getProperty(ConfigurationService.PROPERTY_DEPLOYMENT_MODE));

        String deploymentCode = sapCommerceRestClientService.createDeployment(buildCode, environment, dataMigrationMode, deploymentMode);
//        String deploymentCode = "95979";

        log("Deployment successfully started with deploymentCode " + deploymentCode);

        // Check deployment progress
        String deploymentStatus = null;
        Object deploymentProgress = null;
        while(true) {
            Map deploymentInfo = sapCommerceRestClientService.getDeploymentProgress(deploymentCode);
            String newDeploymentStatus = deploymentInfo.get("deploymentStatus").toString();
            Object newDeploymentProgress = deploymentInfo.get("percentage");

            if(!Objects.equals(deploymentStatus, newDeploymentStatus) || !Objects.equals(deploymentProgress, newDeploymentProgress)) {
                log("Deployment status: " + newDeploymentStatus + " (" + newDeploymentProgress + "%)");
                deploymentStatus = newDeploymentStatus;
                deploymentProgress = newDeploymentProgress;
            }
            if(DEPLOYMENT_STATUS_FAILED.equals(deploymentStatus)) {
                log(deploymentInfo.toString());
                return;
            }
            if(DEPLOYMENT_EXIT_STATUS.contains(deploymentStatus)) {
                break;
            }
            Thread.sleep(10000);
        }
    }

    private String generateBuildName() {
        String dateToString = new SimpleDateFormat("yyyyMMdd").format(new Date());
        String buildNameFormat = configurationService.getProperty(ConfigurationService.PROPERTY_BUILD_NAME_FORMAT);
        return MessageFormat.format(buildNameFormat, dateToString);
    }

    private String formatTime() {
    	return SimpleDateFormat.getTimeInstance(DateFormat.MEDIUM).format(new Date());
	 }

	 private void log(String message) {
		 System.out.println(formatTime() + " - " + message);
	 }
}
