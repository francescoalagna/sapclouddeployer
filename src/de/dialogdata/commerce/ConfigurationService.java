package de.dialogdata.commerce;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigurationService extends Properties {
	public static final String PROPERTY_SUBSCRIPTION_ID = "cloud.subscription.id";
	public static final String PROPERTY_API_TOKEN = "cloud.api.token";
	public static final String PROPERTY_BUILD_NAME_FORMAT = "cloud.build.name.format";
	public static final String PROPERTY_BUILD_BRANCH = "cloud.build.branch";
	public static final String PROPERTY_DEPLOYMENT_ENVIRONMENT = "cloud.deployment.environment";
	public static final String PROPERTY_DEPLOYMENT_DATAMIGRATION_MODE = "cloud.deployment.data.migration.mode";
	public static final String PROPERTY_DEPLOYMENT_MODE = "cloud.deployment.mode";

	private static final String DEFAULT_TARGET_BRANCH = "develop";
	private static final String DEFAULT_BUILD_NAME_FORMAT = "{0}_develop_rollout";
	private static final String DEFAULT_TARGET_ENVIRONMENT = "d1";
	private static final String DEFAULT_DATA_MIGRATION_MODE = "UPDATE";
	private static final String DEFAULT_DEPLOYMENT_MODE = "RECREATE";
	private static final String CONFIG_FILE = "config.properties";

	public ConfigurationService() throws IOException {
		super(createDefaults());

		try(InputStream inputStream = Main.class.getClassLoader().getResourceAsStream(CONFIG_FILE)) {
			if(inputStream != null) {
				this.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + CONFIG_FILE + "' not found in the classpath");
			}
		}
	}

	private static Properties createDefaults() {
		Properties defaults = new Properties();
		defaults.setProperty(PROPERTY_BUILD_NAME_FORMAT, DEFAULT_BUILD_NAME_FORMAT);
		defaults.setProperty(PROPERTY_BUILD_BRANCH, DEFAULT_TARGET_BRANCH);
		defaults.setProperty(PROPERTY_DEPLOYMENT_ENVIRONMENT, DEFAULT_TARGET_ENVIRONMENT);
		defaults.setProperty(PROPERTY_DEPLOYMENT_DATAMIGRATION_MODE, DEFAULT_DATA_MIGRATION_MODE);
		defaults.setProperty(PROPERTY_DEPLOYMENT_MODE, DEFAULT_DEPLOYMENT_MODE);
		return defaults;
	}
}
